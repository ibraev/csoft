import { Component, OnInit } from '@angular/core';
import {Films} from '../films';
import {FilmsService} from '../films.service';

@Component({
  selector: 'app-heroes',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {
  films: Films[];

  constructor(private filmService: FilmsService) { }
  ngOnInit(): void {
    this.getFilms();
  }

  getFilms(): void {
    this.filmService.getFilms()
      .subscribe(films => this.films = films);
  }
  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.filmService.addFilm({ name } as Films)
      .subscribe(film => {
        this.films.push(film);
      });
  }
  delete(film: Films): void {
    this.films = this.films.filter(f => f !== film);
    this.filmService.deleteFilm(film).subscribe();
  }
}
