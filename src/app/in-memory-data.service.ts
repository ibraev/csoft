import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const films = [
      { id: 1, name: 'Avatar' },
      { id: 2, name: 'Titanic' },
      { id: 3, name: 'Spider-man' },
      { id: 4, name: 'Iron-man' },
      { id: 5, name: 'Joker' },
      { id: 6, name: 'Angers' },
      { id: 7, name: 'Game of Thrones' },
      { id: 8, name: 'Game' },
      { id: 9, name: 'Jeki Chan' },
      { id: 10, name: 'Kung-fu Panda' }
    ];
    return {films};
  }

}
