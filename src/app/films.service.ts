import { Injectable } from '@angular/core';
import { Films } from './films';

import {Observable, of} from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {
  private filmsUrl = 'api/films';
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }
  constructor(private http: HttpClient ) { }
  getFilms(): Observable<Films[]> {
    return this.http.get<Films[]>(this.filmsUrl)
      .pipe(
        tap(_ => console.log('fetched films')),
        catchError(this.handleError<Films[]>('getFilms', []))
      );
  }

  addFilm(film: Films): Observable<Films> {
    return this.http.post<Films>(this.filmsUrl, film, this.httpOptions).pipe(
      tap((newFilm: Films) => console.log(` id=${newFilm.id}`)),
      catchError(this.handleError<Films>('add'))
    );
  }


  deleteFilm(film: Films | number): Observable<Films> {
    const id = typeof film === 'number' ? film : film.id;
    const url = `${this.filmsUrl}/${id}`;

    return this.http.delete<Films>(url, this.httpOptions).pipe(
      tap( () => console.log(`deleted  id=${id}`)),
      catchError(this.handleError<Films>('delete'))
    );
  }
}
